$rg='rg-team-2-sep-ado-training-we'

$job='rk-yaml.sqlweb.' + ((Get-Date).ToUniversalTime()).tostring("yyyyMMdd.HHmm")
$template='.\parts-unlimited-yaml\azuredeploy-sqlweb.json'
$params='.\parts-unlimited-yaml\azuredeploy-sqlweb.parameters.json'
az deployment group create --name $job --resource-group $rg --template-file $template --parameters "@$params" --parameters projectName='rk-pul-2'

